""" Functions """
# pp = Pretty print, just gives you nice e+ values instead of 
# verbose large numbers
def pp(val):
    print("{:e}".format(val))
    return

# starLifetime uses eq (5.1) from HW5 to determine a star's lifetime. 
# Note that it assumes a 0.007 efficency rate, and 10% of a star's
# mass used for nuclear fusion. 
def starLifetime(M, A, a):
    
    # Top term 0.007(0.1M)c^2
    c2 = pow(c,2)
    topVar = 0.007 * 0.1 * M
    finalTop = c2 * topVar * Msun

    print("c2: {}\nxMsun: {}\nfinal top term: {}gcm2s-2".format(c2,topVar,finalTop))

    # Bottom term Lsun * A * (M / Msun)^alpha
    constSide = A * pow(M,a)
    finalBot = Lsun * constSide
    print("constSide: {}\nfinalBot: {}gcm2s-1\n".format(constSide,finalBot))

    tn = finalTop / finalBot
    print("Lifetime of star with M = {}Msun, A = {}, alpha = {}\n".format(M,A,a))
    print(tn,"s")
    return tn

# starNuclearFusion uses Ex (9.3) from the book to find the total amoung
# of energy that an A star releases through nuclear fusion. 
# Assumes 0.007 efficiency rate, 10% of mass used. 
def starNuclearFusion(M):

    E = 0.007 * 0.1 * (M * Msun) * pow(c,2)
    m = M * Msun

    print("mVal: {}\nc2: {}\n".format(m,pow(c,2)))
    print("final E: {}\n".format(E))
    return E
    

