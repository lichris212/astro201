"""CONSTANTS"""
# You MUST be aware of units when you use these constants.
# There's no justification for using classes here--do the units by hand. 

# speed of light
# cm s^-1
c = 2.99792456 * pow(10,10)

# gravitation constant
# cm^3 g^-1 s^-2
G = 6.6732 * pow(10,-8)

# Boltzmann constant
# erg K^-1
k = 1.3806 * pow(10,-16)

# Planck's constant
# ergs
h = 6.6262 * pow(10,-27)

# Stefan-Boltzmann constant
# erg cm^-2 K^-4 s^-1
sbc = 5.6696 * pow(10,-5)

# Wien displacement constant
# cm K
wdc = 2.89789 * pow(10,-1)

#Rydberg constant
# cm^-1
R = 1.097373 * pow(10,5)

# Avogadro's number
# mol^-1
Na = 6.022169 * pow(10,23)

# Atomic mass unit
# g
u = 1.66053 * pow(10,-24)

# Proton mass
# g
mp = 1.6726 * pow(10,-24)

# Neutron mass
# g
mn = 1.6749 * pow(10,-24)

# Electron mass
me = 9.1096 * pow(10,-28)

# Hydrogen mass
mH = 1.6735 * pow(10,-24)

# proton charge
# esu
e = 4.8033 * pow(10,-10)

# Bohr radius
# cm
br = 5.29177 * pow(10,-9)

# astronomical unit (distance to the sun)
# cm
AU = 1.4959789 * pow(10,13)

# Parsec in cm
pc = 3.0856 * pow(10,18)

# Light year
# cm
ly = 9.4605 * pow(10,17)

# solar mass
# g
Msun = 1.9891 * pow(10,33)

# solar radius
# cm
Rsun = 6.9598 * pow(10,10)

# solar luminosity
# erg s^-1
Lsun = 3.83 * pow(10,33)

# Earth mass
# g
Mearth = 5.977 * pow(10,27)

# Earth radius
# cm
Rearth = 6.37817 * pow(10,8)

# Earth-moon distance
# cm
REM = 3.84403 * pow(10,10)

# Moon mass
# g
Mmoon = 7.35 * pow(10,25)

# Moon radius
# cm
Rmoon = 1.738 * pow(10,8)

# Galactic center-Sun distance
# kpc
R0 = 8.0

# Sun's Galactocentric orbital speed
# km s^-1
vSun = 220



